#pragma once

// Utilites library
#include <bitset>
#include <chrono>
#include <functional>
#include <type_traits>

// Strings library
#include <string>

// Containers library
#include <array>
#include <deque>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <unordered_map>
#include <vector>

// Algorithms library
#include <algorithm>

// Numerics library
#include <numeric>
#include <random>
