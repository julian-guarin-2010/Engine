#pragma once

namespace engine {

enum class TextureType {
    eNone,
    eDiffuse,
    eSpecular,
    eNormals,
    eUnknown
};

}  // namespace engine
